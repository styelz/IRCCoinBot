/* ----------------------------------------------------------------------------

    IRCCoinBot
    GitLab: <https://gitlab.com/MrFry/IRCCoinBot>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

   ------------------------------------------------------------------------- */

// irc and other variables
var settingsPath = "";
var saveInterval = 60 * 15; // seconds
var builtInAdmins = ["mrfry", "pbp_b"]; // there must be at least one admin
var channelsToJoin = ["#yo"];
var host = 'irc.sub.fm';
var botName = "nab2k";
var muted = false;

// game variables
var starValue = 420;
var minMinigTime = 1; // minutes
var maxMiningTime = 3; // minutes
var rollWinAmm = 40;
var starIcon = " 🌟";

// node.js stuff
var isPrivate = false;
var tChan = "";
var fs = require('fs');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var stdin = process.openStdin();

var helpString =
	"Hello! Im totally not a bot. Commands:\n!minecoin, !ls, !gamble [ammount > 0|all|star|%total<1] !trust [who] !trust/betray [who] !steal [from] [ammount > 0] !catch [who] !steals !donate [who] [ammount > 0] !getalias !myname !lock [ammount > 0] !unlock [ammount > 0] \nOther commands: !cc !commands";

// setTimeout index Array
var tt = [];

// Init stuff
LoadSettings();

// ----------------------------------------------------------------------------
//
// 	Program events
//
// ----------------------------------------------------------------------------

// console input
stdin.addListener("data", function(d) {
	var input = d.toString().trim();

	if (input == "save") {
		SaveSettings();
	}
	if (input == "load") {
		LoadSettings();
	}
	if (input == "kcreset") {
		KcReset();
	}
});

// saving interval
tt.save_settings = {};
tt.save_settings.id = setInterval(function() {
	SaveSettings();
	tt.save_settings.time = Date.now() + (saveInterval * 1000);
}, saveInterval * 1000);
tt.save_settings.time = Date.now() + (saveInterval * 1000);

// sigint event
process.on('SIGINT', function() {
	Log('\nCtrl-C...Saving Settings');
	SaveSettings(true);
	process.exit(2);
});

// ----------------------------------------------------------------------------
//
// 	IRC Client
//
// ----------------------------------------------------------------------------

// Stuff I copy-pasted from coffea wiki
Log("Connecting to " + host + " as " + botName);
client = require('coffea')({
	host: host,
	port: 6667,
	ssl: false,
	ssl_allow_invalid: true,
	prefix: '!',
	nick: botName,
	username: botName,
	realname: botName,
	nickserv: {
		username: botName
	},
	throttling: 250
});


// ----------------------------------------------------------------------------
//
// 	IRC events
//
// ----------------------------------------------------------------------------

client.on('motd', function(event) {
	Log("Connected! - Joining channels: " + channelsToJoin);
	client.join(channelsToJoin, event.network);
});

client.on('message', function(event) {
	Log('[' + event.channel.getName() + '] ' + event.user.getNick() + ': ' + event.message);
	try {
		var reply = Reply(event);
		tChan = event.channel.getName();
		if (!EmptyOrWhiteSpace(reply.resp)) {
			if (reply.isPrivate)
				SendPrivateNotice(event, reply.resp);
			else
				SendMessage(event, reply.resp);
		}
	} catch (e) {
		dumpError(e);
	}
});

client.on('privatemessage', function(event) {
	Log('[PrivateMessage] ' + event.user.getNick() + ': ' + event.message);
	try {
		var reply = Reply(event);
		if (!EmptyOrWhiteSpace(reply.resp)) {
			if (event.user.getNick() != "d0nkey")
				SendPrivateNotice(event, reply.resp);
			else
				SendMessage(event, reply.resp);
		}
	} catch (e) {
		Log(e);
	}
});

client.on('error', function(err, event) {
	Log("Basic IRC Bot Error: \n" + event.name + " " + err.stack);
});

// ----------------------------------------------------------------------------
//
// 	stuff cus coffea
//
// ----------------------------------------------------------------------------

function Bold(value) {
	return "\002" + value + "\002";
}

function SendMessage(event, reply) {
	Log("--------------------------------------------------");
	Log(event.message);
	reply = (event.user.getNick() != "d0nkey") ?
		Bold(event.user.getNick()) + ": " + reply :
		reply;
	client.send(tChan, reply, event.network);
	settings.sentMessages++;
	Log("Reply: #" + settings.sentMessages);
	Log("[" + tChan + "] " + reply);
}

function SendPrivateNotice(event, reply) {
	Log("--------------------------------------------------");
	Log('[Private] ' + event.user.getNick() + ": " + event.message);
	client.notice(event.user.getNick(), reply, event.network);
	settings.sentMessages++;
	Log("Reply: #" + settings.sentMessages);
	Log("<" + event.user.getNick() + "> " + reply);
}

var settings;
var steal = [];

function dumpError(err) {
	if (typeof err === 'object') {
		if (err.message) {
			console.log('\nMessage: ' + err.message);
		}
		if (err.stack) {
			console.log('\nStacktrace:');
			console.log('====================');
			console.log(err.stack);
		}
	} else {
		console.log('dumpError :: argument is not an object');
	}
}

function GetDateString() {
	var m = new Date();
	return m.getUTCFullYear() + "/" +
		("0" + (m.getUTCMonth() + 1)).slice(-2) + "/" +
		("0" + m.getUTCDate()).slice(-2) + " " +
		("0" + m.getUTCHours()).slice(-2) + ":" +
		("0" + m.getUTCMinutes()).slice(-2) + ":" +
		("0" + m.getUTCSeconds()).slice(-2);
}


function Log(s) {
	console.log(GetDateString() + "> " + s);
}

function Reset() {
	Log("RESETTING");
	settings.trustlist = [];
	settings.bank = [];
	settings.history = [];
	settings.sentMessages = 0;
	settings.admins = builtInAdmins;
	steal = [];
}

function KcReset() {
	for (var i = 0; i < settings.bank.length; i++)
		settings.bank[i].enabled = true;
	steal = [];
	// Clear all running Timers except save_settings
	for (timer in tt) {
		if (timer != 'save_settings') {
			clearTimeout(tt[timer].id);
			delete tt[timer];
		}
	}
	Log("basic stuff reset.");
}

function SetStringToFile(filePath, text) {
	fs.writeFileSync(filePath, text);
	Log("Saved file: " + filePath);
}

function GetStringFromFile(filePath, todo) {
	fs.readFile(filePath, {
		encoding: 'utf-8'
	}, function(err, data) {
		if (!err) {
			todo(data);
		} else {
			Log(err);
		}
	});
}

function SaveSettings(force) {
	SetStringToFile(settingsPath + "set", JSON.stringify(settings));
}

function LoadSettings() {
	GetStringFromFile(settingsPath + "set", function(s) {
		try {
			settings = JSON.parse(s);
			KcReset();
		} catch (e) {
			Log("Error parsing settings data! If this is the first run, its kinda normal.");
			settings = {};
			settings.trustlist = [];
			settings.bank = [];
			settings.history = [];
			settings.sentMessages = 0;
			settings.admins = builtInAdmins;
			SaveSettings();
		}
	});
}


function SendXMLHttpRequest(urlToGet, todo) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			isOngoingRequest = false;
			var resp = xhttp.responseText;
			todo(resp, urlToGet);
		}
	};
	xhttp.onerror = function(e) {
		Log("XMLHTTP error");
		Log(e);
	};
	xhttp.open("GET", urlToGet, true);
	xhttp.send();
}

// ----------------------------------------------------------------------------
//
// 	ORIGINAL COIN FUNCTIONS
//
// ----------------------------------------------------------------------------

function EmptyOrWhiteSpace(value) // if the value is empty, return true
{
	// replaces all \n's (newlines) and all \s's (spaces) with "".
	return (value.replace(/[\n\s]+/g, "") == "") ? true : false;
}

function GetRandom(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

function GetChance(amm) {
	return Math.random() < amm / 100;
}

function GetRandomMusic() // gets a random youtube link from the given list
{
	var music = ["TSaC0iSA0Is", "Eetky8s4_6U", "QFxdkHRpz7Q", "XHaSc92HmvY", "4hEkroCbYew"];
	var index = Math.floor((Math.random() * music.length) + 0);
	var musicURL = "https://www.youtube.com/watch?v=" + music[index];
	return musicURL;
}

// -------------------------------------------------------------------------
//
// 	ORIGINALS
//
// -------------------------------------------------------------------------

var defCommands = [];
var kcCommands = [];

function Reply(event) // returns a string, with the response for the lastMessage, and does various actions depending on the message.
{

	var r = [];
	var lowercaseMessage = event.message.toLowerCase(); // lowercasing the message, so it works regardless of case
	var sender = event.user.nick;
	var lastMessage = event.message;

	//------------------------------------------------------------------

	if (muted && !lowercaseMessage.includes("mute"))
		return "";

	if (defCommands.length == 0)
		defCommands = InitCommands();
	if (kcCommands.length == 0)
		kcCommands = InitKCCommands();

	var allCommands = [defCommands, kcCommands];

	isPrivate = false;

	function GetReply(currM) {
		if (currM.length == 0)
			return;

		var command = currM[0];

		for (var i = 0; i < allCommands.length; i++) {
			for (var j = 0; j < allCommands[i].length; j++) {
				var k = 0;
				while (k < allCommands[i][j].command.length && command != (allCommands[i][j].command[k]))
					k++;
				if (k < allCommands[i][j].command.length) {
					r.push(allCommands[i][j].action(event, currM.join(' '), sender.toLowerCase(), settings.bank,
						settings.trustlist, steal, settings.admins));
					return;
				}
			}
		}
	}

	var msgArray = lastMessage.split(' ');
	var currMsgArray = lastMessage.split(' ');
	for (var l = 0; l < msgArray.length; l++) {
		GetReply(currMsgArray);
		currMsgArray.splice(0, 1);
	}

	if (lowercaseMessage.includes("commands")) {
		isPrivate = true;
		var c = [];
		c.push("Default:");
		var temp = [];
		for (var i = 0; i < defCommands.length; i++)
			temp.push(defCommands[i].command.toString().trim());
		c.push(temp.join(" "));
		c.push("coin commands:");
		temp = [];
		for (var i = 0; i < kcCommands.length; i++)
			temp.push(kcCommands[i].command.toString().trim());
		c.push(temp.join(" "));
		r.push(c.join("\n").trim());
	}

	response = r.join("\n");

	if (lowercaseMessage.includes("fullcaps"))
		response = response.toUpperCase();

	if (lowercaseMessage.includes("reverse") || lowercaseMessage.includes("revert")) {
		var i = response.length - 1;
		var result = "";
		while (i >= 0) {
			result += response[i];
			i--;
		}
		response = result;
	}
	return {
		resp: response,
		isPrivate: isPrivate
	}; // returning the response
	//-----------------------------------------------------------------------------
}

function GetParameter(message, command, toLowerCase) // gets a parameter from a line of string
{
	// parameters with space are seperated. if '' is used, it is not seperated
	var msg = RemoveUnnecesarySpaces(message.toLowerCase().replace(/\n/g, "")); // sets the message to lowercase
	if (toLowerCase !== null && toLowerCase !== undefined && toLowerCase) // if the toLowerCase is set
		msg = message; // then the msg will be the normal version of the message (not lowercase)
	if (msg.includes("\n")) // if the message has line break
		msg = msg.split("\n")[0]; // it gets the first line of it. others will not be checked
	msg = msg.split(" "); // splits the message by spaces
	var parameters = []; // initializes an array
	var i = 0; // initializes loop variabl
	while (!msg[i].toLowerCase().includes(command) && i < msg.length) // goes while the given command is not found
		i++;
	i++; // adds one, so the command is skipped. "i" is now the index of the first parameter
	if (i < msg.length) // if there is text after the command
	{
		var adding = false; // if adding to the current parameter
		while (i < msg.length) // goes through the rest f the message
		{
			var currParam = msg[i]; // current parameter to check

			if (adding) // if adding to the current parameter
				parameters[parameters.length - 1] += " " + currParam.replace(/'/g, ""); // adds to the current parameter, adds a space before, and replaces '-s.
			else // if it doesn't include '
				parameters.push(currParam.replace(/'/g, "")); // adds the parameters to the array, replacins "'" with ""
			if (currParam.includes("'") && currParam.match(/'/g).length == 1) // if the current parameter includes "'", then it sets the adding to its opposite
				adding = !adding;
			i++;
		}
		return parameters; // returns the parameter
	} else // if there is no text after the command
		return []; // returns an empty array
}

function RemoveUnnecesarySpaces(toremove) {
	while (toremove.includes("  ")) // while the text includes double spaces replaces all of them with a single one
		toremove = toremove.replace(/  /g, " ");
	var i = 0;
	while (i < toremove.length && toremove[i] == " ") // removes the last unnecesary space
		i++;
	toremove = toremove.trim(0, i);
	return toremove;
}

function RemoveUnnecesarySpaces(toremove) {
	toremove = toremove.replace(/\s{2,}/g, " ");
	var i = 0;
	while (i < toremove.length && toremove[i] == " ") // removes the last unnecesary space
		i++;
	toremove = toremove.trim(0, i);
	return toremove;
}

function InitCommands() {
	var commands = [];

	// -------------------------------------------------------------------------
	var notabot = {
		command: [botName, "notabot"],
		action: function() {
			return helpString;
		}
	};
	commands.push(notabot);
	// -------------------------------------------------------------------------
	var dailymusic = {
		command: ["!dailymusic"],
		action: function() {
			return GetRandomMusic();
		}
	};
	commands.push(dailymusic);
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!remindme"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var r = "";
			var param = GetParameter(lastMessage, "!remindme");
			if (param !== undefined && !isNaN(param[0])) {
				var defTimeOut = parseInt(param[0]);
				var timeout = defTimeOut * 1000 * 60;
				var message = "random reminder";
				if (param[1] != undefined) {
					message = "";
					var g = 1;
					while (param[g] != undefined) {
						message += param[g] + " ";
						g++;
					}
				}
				if (timeout > 0) {
					var tt_id = "remindme_" + sender + "_" + Math.round(Date.now());
					tt[tt_id] = {};
					tt[tt_id].id = setTimeout(function() {
						SendMessage(event, sender + ": " + message);
						delete tt[tt_id];
					}, timeout);
					tt[tt_id].time = Date.now() + timeout;
					r += "You will be reminded in " + defTimeOut + " minutes with the following message: " +
						message.trim() + ".";
				} else {
					r += "Invalid time";
				}
			} else {
				r += "Invalid input !remindme [minutes] [stuff to remind]";
			}
			return r;
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!mute"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				muted = !muted;
				if (muted)
					return "Bot muted :c";
				else
					return "Bot unmuted c:";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!fiyah"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var r = "";
			var param = GetParameter(lastMessage, "!fiyah");
			if (param !== undefined && !isNaN(param[0])) {
				var count = parseInt(param[0]);
				if (count < 200)
					for (var tevagyabuzikirsz = 0; tevagyabuzikirsz < count; tevagyabuzikirsz++)
						r += "🔥";
				else
					r += "😡";
			} else
				r = "🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥";
			return r;
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!admins"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			return "Admins: " + settings.admins.join(", ");
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!cc"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			isPrivate = true;
			var param = GetParameter(lastMessage, "!cc");
			// https://api.bitfinex.com/v1/symbols/
			// https://api.bitfinex.com/v1/pubticker/etcbtc
			if (param.length >= 1) {
				var name = param[0];
				SendXMLHttpRequest("https://api.bitfinex.com/v1/pubticker/" + name, function(rt, u) {
					try {
						var symbols = JSON.parse(rt);
						var n = u.split('/');
						n = n[n.length - 1];
						SendMessage(event, n + ": " + symbols.low);
					} catch (e) {
						SendMessage(event, "Error parsing recieved data :c");
						Log("Error parsing recieved data :c");
						Log(e);
					}
				});
			} else {
				SendXMLHttpRequest("https://api.bitfinex.com/v1/symbols/", function(respText, url) {
					try {
						var symbols = JSON.parse(respText);
						var commons = ["btcusd", "ltcusd", "xmrusd"];
						var resp = [];
						var respCount = 0;

						function GetCoinStuff(rt, u) {
							try {
								var symbols = JSON.parse(rt);
								var n = u.split('/');
								n = n[n.length - 1];
								n.replace('usd', ': $');
								resp.push(n + "" + symbols.low);
							} catch (e) {
								SendMessage(event, "Error parsing recieved data :c");
								Log("Error parsing recieved data :c");
								Log(e);
							}
							respCount++;
							if (respCount == commons) {
								SendMessage(event, resp.join(", "));
							}
						}

						for (var i = 0; i < commons.length; i++) {
							SendXMLHttpRequest("https://api.bitfinex.com/v1/pubticker/" + commons[i], GetCoinStuff);
						}
					} catch (e) {
						SendMessage(event, "Error parsing symbols :c");
						Log("Error parsing symbols :c");
						Log(e);
					}
				});
			}
		}
	});
	Log("inited default commands");
	return commands;
}

function InitKCCommands() {
	var commands = [];
	// -------------------------------------------------------------------------
	//
	//	 init crap
	//
	// -------------------------------------------------------------------------

	function GetAccountIndexByAlias(alias, bank, getNameToo) {
		var i = 0;
		while (i < bank.length && !bank[i].aliases.includes(alias.toLowerCase()))
			i++;
		if (i < bank.length)
			return i;
		else {
			if (getNameToo) {
				i = 0;
				while (i < bank.length && bank[i].name != alias)
					i++;
				if (i < bank.length)
					return i;
				else
					return -1;
			} else
				return -1;
		}
	}

	function GetNewAccount(name) {
		return {
			name: name,
			ammount: 0,
			enabled: true,
			aliases: [],
			score: 0
		};
	}

	function Addhistory(acc, event, bank) {

		var nowDate = new Date();
		var days = ["Sunday", "Monday", "Tuesda]y", "Wednesday", "Thursday", "Fryday", "Saturday"];
		var d = days[nowDate.getDay()];
		var h = nowDate.getHours();
		var m = nowDate.getMinutes();
		if (m < 10)
			m = 0 + "" + m;
		var s = nowDate.getSeconds();
		if (s < 10)
			s = 0 + "" + s;
		var t = {
			d: d,
			h: h,
			m: m,
			s: s
		};
		settings.history.push({
			user: acc.name,
			time: t,
			amm: acc.ammount + acc.score * starValue,
			event: event
		});
		try {
			if (settings.history.length % 100 == 0)
				SaveFile(JSON.stringify(settings.history));
		} catch (e) {
			Log("history saving error");
		}
	}

	// -------------------------------------------------------------------------
	commands.push({
		command: ["!d0nkwin"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			isPrivate = false;
			if (sender != "d0nkey") return "";
			var r = "";
			var param = GetParameter(lastMessage, "!d0nkwin");
			var index = GetAccountIndexByAlias(param[1].toLowerCase(), bank, true);
			if (index != -1) {
				tChan = param[0];
				var tmpWinAmm = 17 - param[2];
				if (param[2] <= 17) tmpWinAmm += 1;
				else tmpWinAmm = (tmpWinAmm * -1) + 1;
				tmpWinAmm *= rollWinAmm; 
				r = Bold(param[1]) + ": Congrats on your win! You got " + tmpWinAmm +
					" coinz for this ;)";
				bank[index].ammount += tmpWinAmm;
				return r;
			}
			return r;
		}
	});

	// -------------------------------------------------------------------------
	commands.push({
		command: ["!leave"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				process.exit(2);
				return "goodby";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!kcreset"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				KcReset();
				return "timer, trustlist and stealing reset. probably cus patch.";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!cashreset"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				for (var i = 0; i < bank.Length; i++) {
					bank[i].ammount = 0;
					bank[i].score = 0;
					bank[i].enabled = true;
				}
				return "cash reset";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!save"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			SaveSettings();
			return "Settings saved :)";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!bankreset"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				Reset();
				return "bank reset";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!rmalias"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				var param = GetParameter(lastMessage, "!rmalias");
				if (param.length >= 2) {
					var i = 0;
					while (i < bank.length && bank[i].name != param[0])
						i++;
					if (i < bank.length) {
						var toremove = param[1];
						var index = bank[i].aliases.indexOf(toremove);
						if (index > -1) {
							bank[i].aliases.splice(index, 1);
							return param[1] + " alias removed from" + param[0];
						} else {
							return "No " + param[1] + " alias for " + param[0];
						}
					} else
						return "No such account";
				} else
					return "Bad params! !rmalias <who> <alias>";
			} else
				return "You have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!deluser"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				var param = GetParameter(lastMessage, "!deluser");
				if (param.length >= 1) {
					var index = GetAccountIndexByAlias(param[0], bank, true);
					if (index == -1) {
						return "no such user";
					} else {
						bank.splice(index, 1);
						return param[0] + " deleted";
					}
				} else
					return "bad params";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!alias"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				var param = GetParameter(lastMessage, "!alias");
				if (param.length >= 2) {
					var i = 0;
					while (i < bank.length && bank[i].name != param[0])
						i++;
					if (i < bank.length) {
						bank[i].aliases.push(param[1].toLowerCase());
						return param[1] + " alias added to " + param[0];
					} else
						return "No such user!";
				} else
					return "Bad params! !alias <who> <alias>";
			} else
				return "You have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!delhistory"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				return "DONT DO DIS";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!history"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {

				var result = [];
				var firstrow = "day;time;";
				for (var i = 0; i < bank.length; i++) {
					firstrow += bank[i].name + ";";
				}
				firstrow += "event";
				result.push(firstrow);
				var values = [];
				for (var i = 0; i < bank.length; i++)
					values.push(-1);
				for (var i = 0; i < settings.history.length; i++) {
					var j = 0;
					while (j < bank.length && bank[j].name != settings.history[i].user)
						j++;
					if (j < bank.length) {
						values[j] = settings.history[i].amm;
					} else
						Log("history fukup, name " + settings.history[i].user + " not found!");
					var time = settings.history[i].time;
					var t = time.d + ";" + time.h + ":" + time.m + ":" + time.s;
					result.push(t + ";" + values.join(";") + ";" + settings.history[i].event);
				}

				SetStringToFile(settingsPath + "history", result.join("\n"));
				return "history dumped to file";
			} else
				return "dont use this here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!newacc"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				var param = GetParameter(lastMessage, "!newacc");
				if (param.length >= 1) {
					var name = param[0];
					var i = 0;
					while (i < bank.length && bank[i].name != name)
						i++;
					if (i < bank.length)
						return "This account already exists!";
					else {
						if (EmptyOrWhiteSpace(name))
							return "Cant determine your name :( (" + sender + ")";
						else {
							var account = GetNewAccount(name);
							bank.push(account);
							SaveSettings();
							return "New account with name " + param[0] + " created :3.";
						}
					}
				} else
					return "Bad params. !newacc [name]";
			} else
				return "Dont mess with this, Ask " + admins[0];
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!getalias"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var param = GetParameter(lastMessage, "!getalias");
			if (param != undefined && param.length >= 1) {
				var i = 0;
				while (i < bank.length && bank[i].name != param[0].toLowerCase())
					i++;
				if (i < bank.length) {
					var t = [];
					for (var j = 0; j < bank[i].aliases.length; j++) {
						t.push(bank[i].aliases[j]);
					}
					return param[0] + " known as: " + t.join(", ");
				} else
					return "no such account";
			} else {
				var acc = GetAccountIndexByAlias(sender.toLowerCase(), bank);
				acc = bank[acc];
				var t = [];
				for (var i = 0; i < acc.aliases.length; i++) {
					t.push(acc.aliases[i]);
				}
				return "u known as: " + t.join(", ");
			}
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!minecoin", "!m"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var r = "";
			var i = GetAccountIndexByAlias(sender, bank, true);
			if (i != -1) {
				var currName = bank[i].name;
				if (bank[i].enabled) {
					var amm = GetRandom(-2, 6);
					if (GetRandom(0, 40) == 4) {
						r += "Lucky strike! ";
						amm = (GetRandom(10, 25));
						if (GetChance(15)) {
							amm *= -1;
							r += "Well, not so lucky...";
						}
					}
					bank[i].ammount += amm;
					if (bank[i].ammount < 0) {
						bank[i].ammount = 0;
						amm = 0;
					}
					Addhistory(bank[i], "mine", bank);
					if (amm > 0) {
						r += "You got " + amm + " coin";
						if (amm > 1)
							r += "z";
						r += "!";
					} else if (amm < 0) {
						r += "You lost " + Math.abs(amm) + " coin";
						if (Math.abs(amm) > 1)
							r += "z";
						r += ". better luck next time";
					} else
						r += "You got nothing :|";
					r += " (sum.: " + bank[i].ammount + ")";
					if (GetRandom(0, 10) == 1) {
						r += " Double turn! ;)";
					} else {
						bank[i].enabled = false;
						var increaseWaitingTime = 0;
						if (GetRandom(0, 10) == 4) {
							r += " [bad karma added] ";
							increaseWaitingTime = GetRandom(1, 2);
						}
						var timeOut = GetRandom((minMinigTime + increaseWaitingTime) * 60, (maxMiningTime +
							increaseWaitingTime) * 60) * 1000;
						var tt_id = "minecoin_" + sender;
						tt[tt_id] = {};
						tt[tt_id].id = setTimeout(function() {
							bank[i].enabled = true;
							SendMessage(event, "Your coin is ready for mining!");
							delete tt[tt_id];
						}, timeOut);
						tt[tt_id].time = Date.now() + timeOut;
					}
				} else
					r = "Slow down, you cant mine this fast!";
			} else
				r = "You dont have an account. !newacc to have one.";
			return r;
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!trust"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var plus = 10;
			var param = GetParameter(lastMessage, "!trust");
			if (param.length >= 1) {
				var from = GetAccountIndexByAlias(sender, bank, true);
				var to = GetAccountIndexByAlias(param[0], bank, true);
				if (from != -1 && to != -1 && from != to) // if sender and reciever acc exists, and to acc is not the same
				{
					var i = 0;
					while (i < trustlist.length && !(bank[to].name == trustlist[i]
							.from &&
							bank[from].name ==
							trustlist[i].to))
						i++;
					if (i < trustlist.length) // if there is an active trust going to sender
					{
						bank[from].ammount += plus;
						bank[to].ammount += plus;
						Addhistory(bank[from], "trust", bank);
						Addhistory(bank[to], "trust", bank);
						trustlist.splice(i, 1);
						return "Good job! Both of you guys got " + plus + "-" + plus + " coinz.";
					} else // if there is no active trust going to sender
					{
						var j = 0;
						while (j < trustlist.length && !(bank[from].name == trustlist[
									j]
								.from && bank[to].name ==
								trustlist[j].to))
							j++;
						if (j < trustlist.length)
							return "you already trust " + param[0];
						else {
							if (bank[from].ammount >= plus && bank[to].ammount >= plus) {
								var item = {
									from: bank[from].name,
									to: bank[to].name
								};
								trustlist.push(item);
								var tt_id = "trust_" + from + "_" + to;
								tt[tt_id] = {};
								tt[tt_id].id = setTimeout(function() {
									var l = 0;
									while (l < trustlist.length && !(trustlist[l].from == item.from &&
											trustlist[l].to == item.to))
										l++;
									if (l < trustlist.length)
										trustlist.splice(l, 1);
									delete tt[tt_id];
								}, 1000 * 60);
								tt[tt_id].time = Date.now() + (1000 * 60);
								return sender + " trusts in " + param[0] + ". Does " + param[0] +
									" too? (!trust/betray '" + sender +
									"')";
							} else if (bank[from].ammount < plus)
								return sender + ", you dont have " + plus + " coinz!";
							else if (bank[to].ammount < plus)
								return param[0] + " does not have " + plus + " coinz...";
							else
								return "general fukup";
						}
					}
				} else if (from == -1)
					return "you dont have an account";
				else if (to == -1)
					return "there is no such account";
				else if (from == to)
					return "you cant trust yourself! (never)";
				else
					return "general fuckup";
			} else
				return "!trust [who]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!betray"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var plus = 20;
			var minus = -10;
			var param = GetParameter(lastMessage, "!betray");
			if (param.length >= 1) {
				var from = GetAccountIndexByAlias(sender.toLowerCase(), bank, true);
				var to = GetAccountIndexByAlias(param[0], bank, true);
				if (from != -1 && to != -1) {
					var j = 0;
					// checking if trust exists
					while (j < trustlist.length && !bank[from].aliases.includes(trustlist[
							j].from) && bank[to]
						.aliases
						.includes(trustlist[j].to))
						j++;
					if (j < trustlist.length) // if it does
					{
						bank[from].ammount += plus;
						bank[to].ammount += minus;
						Addhistory(bank[from], "betray", bank);
						Addhistory(bank[to], "betray", bank);
						trustlist.splice(j, 1);
						return sender + ", you betrayed " + param[0] + "! siick! +" + plus + " , and " + minus +
							" coinz!";
					} else {
						return param[0] + " does not trust you. Cant blame him.";
					}
				} else if (from == -1)
					return "you dont have an account!";
				else if (to == -1)
					return "no such account";
				else
					return "OOPSIE WOOPSIE!! Uwu we made a fucky wucky";
			} else
				return "!betray [who]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!lock"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var param = GetParameter(lastMessage, "!lock");
			var acc = GetAccountIndexByAlias(sender, bank, true); // steal to
			if (param[0] == "all") param[0] = Math.floor(bank[acc].ammount / starValue);
			else if (parseFloat(param[0]) < 1 && parseFloat(param[0]) > 0) {
				param[0] = Math.floor(bank[acc].ammount * parseFloat(param[0]) / starValue);
			}
			if (param.length >= 1 && !isNaN(param[0]) && parseInt(param[0]) > 0) {
				var amm = parseInt(param[0]);
				if (acc != -1) {
					if (bank[acc].ammount >= starValue * amm) {
						bank[acc].ammount -= starValue * amm;
						bank[acc].score += amm;
						Addhistory(bank[acc], "lock", bank);
						return "you have locked " + amm * starValue +
							" coinz, you now have " + bank[acc].score + starIcon;
					} else
						return "You dont have " + starValue * amm + " coinz!";
				} else
					return "you dont have an account";
			} else
				return "!lock [ammount > 0]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!unlock"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var param = GetParameter(lastMessage, "!unlock");
			var acc = GetAccountIndexByAlias(sender, bank, true); // steal to
			if (param[0] == "all") param[0] = bank[acc].score;
			else if (parseFloat(param[0]) < 1 && parseFloat(param[0]) > 0) {
				param[0] = Math.floor(bank[acc].score * parseFloat(param[0]));
			}
			if (param.length >= 1 && !isNaN(param[0]) && parseInt(param[0]) > 0) {
				var amm = parseInt(param[0]);
				if (acc != -1) {
					if (bank[acc].score >= amm) {
						bank[acc].ammount += starValue * amm;
						bank[acc].score -= amm;
						Addhistory(bank[acc], "unlock", bank);
						return "you have unlocked " + amm * starValue +
							" coinz, which were deducted from your stars!";
					} else
						return "You dont have " + amm + " stars!";
				} else
					return "you dont have an account";
			} else
				return "!unlock [ammount > 0]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!steals"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var r = "ongoing steals:\n";
			if (steal.length == 0)
				r += "no steals.";
			else {
				for (var i = 0; i < steal.length; i++)
					r += steal[i].to + " is stealing " + steal[i].amm + " coinz from " + steal[i]
					.from +
					", in " +
					Math.round((steal[i].duetimestamp - Date.now()) / 1000) + "/s\n";
			}
			return r;
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!steal"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var param = GetParameter(lastMessage, "!steal");
			if (param.length >= 2 && !isNaN(param[0]) && parseInt(param[0]) > 0) {
				var temp = param[0];
				param[0] = param[1];
				param[1] = temp;
			}
			if (param.length >= 2 && !isNaN(param[1]) && parseInt(param[1]) > 0) {
				var to = GetAccountIndexByAlias(sender, bank, true); // steal to
				var from = GetAccountIndexByAlias(param[0], bank, true); // steal from
				var amm = parseInt(param[1]);
				if (from != -1 && to != -1 && from != to) // if sender and reciever acc exists, and to acc is not the same
				{
					var i = 0;
					while (i < steal.length && bank[to].name != steal[i].to)
						i++;
					if (i < steal.length) { // if there is an active trust going to sender
						return "you are already stealing from someone... dont be that greedy dude";
					} else if (bank[from].ammount - amm >= 0) { // if the target ha enough coinz
						var stealing = {
							from: bank[from].name,
							to: bank[to].name,
							id: 0,
							amm: amm,
							due: ""
						};
						var time = Math.round(GetRandom(20 + (amm / 10000), 40 + (amm / 10000)));
						var ts = Date.now();
						if (GetRandom(0, 10) == 4)
							time = Math.floor(time / 2);
						var tt_id = "steal_" + param[0] + "_" + sender;
						tt[tt_id] = {};
						tt[tt_id].id = setTimeout(function() {
							var tempresponse = "";
							if (bank[from].ammount - amm >= 0) {
								if (GetRandom(0, 20) == 4) {
									tempresponse += "You were unlucky " + bank[to].name +
										", and you got caught - " +
										amm +
										" coinz :P";
									bank[to].ammount -= amm;
									Addhistory(bank[to], "steal catch", bank);
								} else {
									bank[from].ammount -= amm;
									bank[to].ammount += amm;
									Addhistory(bank[from], "steal", bank);
									Addhistory(bank[to], "steal", bank);
									tempresponse += bank[to].name + ", you have stolen " + amm +
										" coinz from " +
										bank[from].name;
								}
							} else {
								tempresponse += bank[from].name + " does not have that much coinz now, " +
									bank[to].name + "...";
							}
							SendMessage(event, tempresponse);
							var k = 0;
							// Removing the stealing
							while (k < steal.length && bank[to].name != steal[k].to)
								k++;
							if (k < steal.length)
								steal.splice(k, 1);
							else
								Log("steal fuckup");
							delete tt[tt_id];
						}, 1000 * time);
						tt[tt_id].time = ts + (1000 * time);
						stealing.id = tt_id;
						var now = new Date(ts);
						var m = Math.floor(time / 60);
						var s = time - (m * 60);
						now.setMinutes(now.getMinutes() + m);
						now.setSeconds(now.getSeconds() + s);
						var atm = now.getMinutes();
						var ats = now.getSeconds();
						if (atm < 10)
							atm = 0 + "" + atm;
						if (ats < 10)
							ats = 0 + "" + ats;
						var at = now.getHours() + ":" + atm + ":" + ats;
						stealing.duetimestamp = ts + (1000 * time);
						stealing.due = at;
						steal.push(stealing);
						var r = "You will steal " + amm + " coin";
						if (amm > 1)
							r += "z";
						r += " in";
						if (m != 0)
							r += " " + m + " minutes";
						if (s != 0)
							r += " " + s + " seconds";
						r += ", except when " + param[0] +
							" !catch-es you!";
						return r;
					} else if (bank[from].ammount - amm < 0)
						return "Leave " + bank[from].name + " alone, he doesnt even have that much coin...";
				} else if (to == -1)
					return "you dont have an account!";
				else if (from == -1)
					return "no such account";
				else if (from == to)
					return "You would steal from yourself? wow";
				else
					return "general fuckup";
			} else if (param.length >= 2 && !isNaN(param[1]) && parseInt(param[1]) < 0)
				return "Positive numbers please";
			else
				return "!steal [ammount > 0] [from]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!catch"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var param = GetParameter(lastMessage, "!catch");
			if (param.length >= 1) {
				var from = GetAccountIndexByAlias(sender.toLowerCase(), bank, true);
				var to = GetAccountIndexByAlias(param[0], bank, true);
				if (from != -1 && to != -1) {
					var i = 0;
					while (i < steal.length && !(steal[i].to == bank[to].name &&
							steal[i].from == bank[from].name))
						i++;
					if (i < steal.length) {
						clearTimeout(tt[steal[i].id].id);
						delete tt[steal[i].id];
						var gained = Math.round(steal[i].amm / 2);
						bank[from].ammount += gained;
						bank[to].ammount -= gained;
						Addhistory(bank[from], "steal catch", bank);
						Addhistory(bank[to], "steal catch", bank);

						var k = 0;
						while (k < steal.length && bank[to].name != steal[k].to)
							k++;
						if (k < steal.length)
							steal.splice(k, 1);
						else
							Log("steal catch fuckup");
						return "You caught " + param[0] + " during stealing. He lost " + gained +
							" coinz for this.";
					} else // else
						return param[0] + " is not stealing from you, stop accusing him.";
				} else if (from == -1)
					return "you dont have an account";
				else if (to == -1)
					return "no such acc";
				else
					return "OOPSIE WOOPSIE!! Uwu we made a fucky wucky";
			} else
				return "!catch [who]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!donate"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var param = GetParameter(lastMessage, "!donate");
			if (param.length >= 2 && !isNaN(param[0]) && parseInt(param[0]) > 0) {
				var temp = param[0];
				param[0] = param[1];
				param[1] = temp;
			}
			if (param.length >= 2 && !isNaN(param[1]) && parseInt(param[1]) > 0) {
				var from = GetAccountIndexByAlias(sender.toLowerCase(), bank, true);
				var to = GetAccountIndexByAlias(param[0], bank, true);
				var amm = parseInt(param[1]);
				if (from != -1 && to != -1 && from != to) {
					if (bank[from].ammount - amm >= 0) {
						bank[from].ammount -= amm;
						Addhistory(bank[from], "donate", bank);
						var r = bank[from].name + " donated " + amm + " coin to " + bank[
							to].name + ".";
						bank[to].ammount += amm;
						Addhistory(bank[to], "donate", bank);
						return r;
					}
				} else if (from == to)
					return "you cant donate to yourself dudel.";
				else if (from == -1)
					return "you dont have an account";
				else if (to == -1)
					return "no such account";
				else
					return "OOPSIE WOOPSIE!! Uwu we made a fucky wucky";
			} else if (!isNaN(param[1]) && parseInt(param[1]) <= 0)
				return "you cant donate negative ammount. lol";
			else
				return "donate [who] [ammount > 0]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!myname"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			return "your name is: " + sender;
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!rename"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			if (admins.includes(sender)) {
				var param = GetParameter(lastMessage, "!rename");
				// if there is at least 2 parameters, the first is neptun or moodle, and the second is a number
				if (param.length >= 2) {
					var i = 0;
					while (i < bank.length && bank[i].name.toLowerCase() != param[0].toLowerCase())
						i++;
					if (i < bank.length) {
						bank[i].name = param[1];
						return param[0] + " renamed to " + param[1] + ".";
					} else
						return "No such user";
				} else
					return "!rename [old] [new]";
			} else
				return "you have no power here";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!gamble", "!gamblel", "!g"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			var param = GetParameter(lastMessage, "!g");
			// if there is at least 2 parameters, the first is neptun or moodle, and the second is a number
			if (param.length >= 1 && ((!isNaN(param[0]) && parseFloat(param[0]) > 0) || param[0] == "all" ||
					param[0] == "half" || param[0] == "star")) {
				var i = GetAccountIndexByAlias(sender, bank, true);
				if (i != -1) {
					var amm = 0;
					if (param[0] == "all")
						amm = bank[i].ammount;
					else if (param[0] == "half")
						amm = Math.floor(bank[i].ammount / 2);
					else if (param[0] == "star" && bank[i].score > 0) {
						bank[i].score -= 1;
						bank[i].ammount += starValue;
						amm = starValue;
					} else if (parseFloat(param[0]) < 1) {
						amm = Math.floor(bank[i].ammount * param[0]);
					} else {
						amm = parseInt(param[0]);
						if (amm < 0) amm = 0;
					}
					if (amm > bank[i].ammount)
						amm = bank[i].ammount;
					if (amm > 0) {
						var chance = GetRandom(-2, 6);
						if (chance > 0) {
							bank[i].ammount += amm;
							Addhistory(bank[i], "gamble", bank);
							r = "Lucky! You won " + amm + " coinz";
						} else {
							bank[i].ammount -= amm;
							Addhistory(bank[i], "gamble", bank);
							r = "Dang! You lost " + amm + " coinz!";
						}
						if (bank[i].ammount == 0)
							r += " doh!";
						r += " (sum.: " + bank[i].ammount + ")";
						return r;
					} else
						return "you dont have coinz to gamble!";
				} else
					return sender + " you dont even have an account. !newacc !";
			} else
				return "!gamble [ammount > 0 | all | star | percent of total < 1 ]";
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!ls"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {

			function GetScoreRow(bank, i, addcomma) {
				var amm = bank[i].ammount;
				var n = bank[i].name;
				var t = n + " | " + amm;
				if (amm == 8)
					t += ")";
				if (amm == 19)
					t += " | ayy lmao";
				if (amm == 420)
					t += " | 8)";
				if (amm == 666)
					t += " | THE DEVIL";
				if (bank[i].score > 0)
					t += " | " + bank[i].score + starIcon;
				if (bank[i].enabled)
					t += " | R!";
				return t;
			}

			var param = GetParameter(lastMessage, "!ls");
			if (param !== undefined && (param[0] == "all" || param[0] == "a")) {
				isPrivate = true;
				var r = [];
				var tempbank = JSON.parse(JSON.stringify(bank));
				for (var i = 0; i < tempbank.length; i++)
					for (var j = i; j < tempbank.length; j++)
						if (tempbank[i].ammount + tempbank[i].score * starValue < tempbank[j].ammount +
							tempbank[j].score *
							starValue) {
							var temp = tempbank[i];
							tempbank[i] = tempbank[j];
							tempbank[j] = temp;
						}
				for (var i = 0; i < tempbank.length; i++) {
					r.push((i + 1) + ".: " + GetScoreRow(tempbank, i, true));
				}
				if (r.length == 0)
					return "There are no accounts (yet)";
				else
					return r.join("\n");
			} else if (param.length == 1) {
				var i = GetAccountIndexByAlias(param[0], bank, true);
				if (i != -1) {
					return GetScoreRow(bank, i, false);
				} else {
					return "That account doesn't exist";
				}
			} else {
				var i = GetAccountIndexByAlias(sender, bank, true);
				return GetScoreRow(bank, i, false);
			}
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!timers"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			isPrivate = true;
			var r = "current timers:\n";
			if (Object.keys(tt).length == 0)
				r += "no timers.";
			else {
				for (var key in tt)
					r += key + ": " + Math.round((tt[key].time - Date.now()) / 1000) + "/s remain\n";
			}
			return r;
		}
	});
	// -------------------------------------------------------------------------
	commands.push({
		command: ["!help"],
		action: function(event, lastMessage, sender, bank, trustlist, steal, admins) {
			isPrivate = true;
			return helpString;
		}
	});
	// -------------------------------------------------------------------------
	Log("inited coin commands");
	return commands;
}
