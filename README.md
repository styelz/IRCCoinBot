# IRCCoinBot
  Simple IRC bot using Coffea, written in Node.js  

  On some older versions of nodejs you need to add the `--harmony_array_includes` option at startup:  
```node --harmony_array_includes ircbot.js```

